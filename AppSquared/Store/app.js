//Express is a minimal and flexible Node.js web application framework that provides a robust set of featuresfor web and mobile applications
var express = require('express');
var app = express();

var config = require('./config.js')
//Establish connection to the MySQL database
var mysql = require('mysql');
var con = mysql.createConnection({
    host : config.db_host,
    user : config.db_user,
    password : config.db_password,
    database : config.db_database
});
con.connect(function(err) {
	if (err) {
		console.log('Error connection to db');
		return;
	}
	console.log('Connection established.');
});

//Important to make POST work
// npm install express-cors 
var cors = require('express-cors')
 
app.use(function(req,res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS");
    res.header("Access-Control-Max-Age", "1000"); 
    cors({
        allowedOrigins: [ 'localhost:8080' ] });
    next();
});

 /* Item: CRUD GET ALL*/
app.get('/Item/find/all', function(req,res) {
	totalCount = 0;
	count = 0;
	var query = "select Item.ID , Item.Description , Item.Price , Item.Title , Item.Quantity  from Item as Item";
	con.query(query, function(err, rows0,fields) {
		if(err) throw err;
		res.jsonp(rows0);
	});
});


 /* Item: CRUD GET, DELETE, UPDATE, POST BY ID*/
app.get('/Item/find/:id', function(req,res) {
	totalCount = 0;
	count = 0;
	var query = "select Item.ID , Item.Description , Item.Price , Item.Title , Item.Quantity  from Item as Item where id = ?";
	con.query(query, req.params.id, function(err, rows0,fields) {
		if(err) throw err;
		res.jsonp(rows0);
	});
});

app.get('/Item/delete/:id', function(req,res,next) {
	var id = req.params.id;
	con.query('DELETE FROM Item WHERE id = ?', id, function(err,rows) {
		if (err) { console.log('Error deleting'); }
		else {
			console.log('in delete success');
			res.jsonp(rows);
		}
	});
});

app.post('/Item/update/:id', function(req,res,next) {
	var id = req.params.id;
	var Item = '';
	req.on('data', function(data) {
		Item += data;
	});
	req.on('end', function() {
		Item = JSON.parse(Item);
		var data = {
			Description : Item.Description ,
			Price : Item.Price ,
			Title : Item.Title ,
			Quantity : Item.Quantity 
		};
		con.query('UPDATE Item set ? WHERE id = ?', [data, id],function(err,rows) {
			if (err) { console.log('Error updating'); }
			else {
				console.log('in update success');
				res.jsonp(true);
			}
		});
	});
});

app.post('/Item/add', function(req,res) {
	var Item = '';
	req.on('data', function(data) {
		Item += data;
	});
	req.on('end', function() {
		Item = JSON.parse(Item);
		var data = {
			Description : Item.Description ,
			Price : Item.Price ,
			Title : Item.Title ,
			Quantity : Item.Quantity 
		};
		con.query('INSERT INTO Item set ? ', data, function(err,rows) {
			if (err) { console.log('Error inserting'); }
			else {
				console.log('in insert success');
				res.jsonp(true);
			}
		});
	});
});


 /* User: CRUD GET ALL*/
app.get('/User/find/all', function(req,res) {
	totalCount = 0;
	count = 0;
	var query = "select User.ID , User.OAuthID , User.Info , User.Name  from User as User";
	con.query(query, function(err, rows0,fields) {
		if(err) throw err;
		res.jsonp(rows0);
	});
});


 /* User: CRUD GET, DELETE, UPDATE, POST BY ID*/
app.get('/User/find/:id', function(req,res) {
	totalCount = 0;
	count = 0;
	var query = "select User.ID , User.OAuthID , User.Info , User.Name  from User as User where id = ?";
	con.query(query, req.params.id, function(err, rows0,fields) {
		if(err) throw err;
		res.jsonp(rows0);
	});
});

app.get('/User/delete/:id', function(req,res,next) {
	var id = req.params.id;
	con.query('DELETE FROM User WHERE id = ?', id, function(err,rows) {
		if (err) { console.log('Error deleting'); }
		else {
			console.log('in delete success');
			res.jsonp(rows);
		}
	});
});

app.post('/User/update/:id', function(req,res,next) {
	var id = req.params.id;
	var User = '';
	req.on('data', function(data) {
		User += data;
	});
	req.on('end', function() {
		User = JSON.parse(User);
		var data = {
			OAuthID : User.OAuthID ,
			Info : User.Info ,
			Name : User.Name 
		};
		con.query('UPDATE User set ? WHERE id = ?', [data, id],function(err,rows) {
			if (err) { console.log('Error updating'); }
			else {
				console.log('in update success');
				res.jsonp(true);
			}
		});
	});
});

app.post('/User/add', function(req,res) {
	var User = '';
	req.on('data', function(data) {
		User += data;
	});
	req.on('end', function() {
		User = JSON.parse(User);
		var data = {
			OAuthID : User.OAuthID ,
			Info : User.Info ,
			Name : User.Name 
		};
		con.query('INSERT INTO User set ? ', data, function(err,rows) {
			if (err) { console.log('Error inserting'); }
			else {
				console.log('in insert success');
				res.jsonp(true);
			}
		});
	});
});


 /* Similar: CRUD GET ALL*/
app.get('/Similar/find/all', function(req,res) {
	totalCount = 0;
	count = 0;
	var query = "select Similar.ID , Similar.Similar1 , Similar.Similar2  from Similar as Similar";
	con.query(query, function(err, rows0,fields) {
		if(err) throw err;
		totalCount += rows0.length;
		rows0.forEach(function(row) { 
			query = "select Item.ID , Item.Description , Item.Price , Item.Title , Item.Quantity ";
			query += "from  Item as Item  inner join Similar as Similar ";
			query += "where  Item.id = Similar.Similar1 and Similar.id = ?";
			con.query(query, req.params.id, function(err, rows1,fields) {
				if (err) throw err;
				row.Similar1 = rows1[0];
				rows0.forEach(function(row) { 
					query = "select Item.ID , Item.Description , Item.Price , Item.Title , Item.Quantity ";
					query += "from  Item as Item  inner join Similar as Similar ";
					query += "where  Item.id = Similar.Similar2 and Similar.id = ?";
					con.query(query, req.params.id, function(err, rows2,fields) {
						if (err) throw err;
						row.Similar2 = rows2[0];
						res.jsonp(rows0);
					});
				});
			});
		});
	});
});


 /* Similar: CRUD GET, DELETE, UPDATE, POST BY ID*/
app.get('/Similar/find/:id', function(req,res) {
	totalCount = 0;
	count = 0;
	var query = "select Similar.ID , Similar.Similar1 , Similar.Similar2  from Similar as Similar where id = ?";
	con.query(query, req.params.id, function(err, rows0,fields) {
		if(err) throw err;
		totalCount += rows0.length;
		rows0.forEach(function(row) { 
			query = "select Item.ID , Item.Description , Item.Price , Item.Title , Item.Quantity ";
			query += "from  Item as Item  inner join Similar as Similar ";
			query += "where  Item.id = Similar.Similar1 and Similar.id = ?";
			con.query(query, req.params.id, function(err, rows1,fields) {
				if (err) throw err;
				row.Similar1 = rows1[0];
				rows0.forEach(function(row) { 
					query = "select Item.ID , Item.Description , Item.Price , Item.Title , Item.Quantity ";
					query += "from  Item as Item  inner join Similar as Similar ";
					query += "where  Item.id = Similar.Similar2 and Similar.id = ?";
					con.query(query, req.params.id, function(err, rows2,fields) {
						if (err) throw err;
						row.Similar2 = rows2[0];
						res.jsonp(rows0);
					});
				});
			});
		});
	});
});

app.get('/Similar/delete/:id', function(req,res,next) {
	var id = req.params.id;
	con.query('DELETE FROM Similar WHERE id = ?', id, function(err,rows) {
		if (err) { console.log('Error deleting'); }
		else {
			console.log('in delete success');
			res.jsonp(rows);
		}
	});
});

app.post('/Similar/update/:id', function(req,res,next) {
	var id = req.params.id;
	var Similar = '';
	req.on('data', function(data) {
		Similar += data;
	});
	req.on('end', function() {
		Similar = JSON.parse(Similar);
		var data = {
			Similar1 : Similar.Similar1 ,
			Similar2 : Similar.Similar2 
		};
		con.query('UPDATE Similar set ? WHERE id = ?', [data, id],function(err,rows) {
			if (err) { console.log('Error updating'); }
			else {
				console.log('in update success');
				res.jsonp(true);
			}
		});
	});
});

app.post('/Similar/add', function(req,res) {
	var Similar = '';
	req.on('data', function(data) {
		Similar += data;
	});
	req.on('end', function() {
		Similar = JSON.parse(Similar);
		var data = {
			Similar1 : Similar.Similar1 ,
			Similar2 : Similar.Similar2 
		};
		con.query('INSERT INTO Similar set ? ', data, function(err,rows) {
			if (err) { console.log('Error inserting'); }
			else {
				console.log('in insert success');
				res.jsonp(true);
			}
		});
	});
});


 /* Review: CRUD GET ALL*/
app.get('/Review/find/all', function(req,res) {
	totalCount = 0;
	count = 0;
	var query = "select Review.ID , Review.CreatedBy , Review.Content , Review.CreateDate , Review.OnItem  from Review as Review";
	con.query(query, function(err, rows0,fields) {
		if(err) throw err;
		totalCount += rows0.length;
		rows0.forEach(function(row) { 
			query = "select User.ID , User.OAuthID , User.Info , User.Name ";
			query += "from  User as User  inner join Review as Review ";
			query += "where  User.id = Review.CreatedBy and Review.id = ?";
			con.query(query, req.params.id, function(err, rows1,fields) {
				if (err) throw err;
				row.CreatedBy = rows1[0];
				rows0.forEach(function(row) { 
					query = "select Item.ID , Item.Description , Item.Price , Item.Title , Item.Quantity ";
					query += "from  Item as Item  inner join Review as Review ";
					query += "where  Item.id = Review.OnItem and Review.id = ?";
					con.query(query, req.params.id, function(err, rows2,fields) {
						if (err) throw err;
						row.OnItem = rows2[0];
						res.jsonp(rows0);
					});
				});
			});
		});
	});
});


 /* Review: CRUD GET, DELETE, UPDATE, POST BY ID*/
app.get('/Review/find/:id', function(req,res) {
	totalCount = 0;
	count = 0;
	var query = "select Review.ID , Review.CreatedBy , Review.Content , Review.CreateDate , Review.OnItem  from Review as Review where id = ?";
	con.query(query, req.params.id, function(err, rows0,fields) {
		if(err) throw err;
		totalCount += rows0.length;
		rows0.forEach(function(row) { 
			query = "select User.ID , User.OAuthID , User.Info , User.Name ";
			query += "from  User as User  inner join Review as Review ";
			query += "where  User.id = Review.CreatedBy and Review.id = ?";
			con.query(query, req.params.id, function(err, rows1,fields) {
				if (err) throw err;
				row.CreatedBy = rows1[0];
				rows0.forEach(function(row) { 
					query = "select Item.ID , Item.Description , Item.Price , Item.Title , Item.Quantity ";
					query += "from  Item as Item  inner join Review as Review ";
					query += "where  Item.id = Review.OnItem and Review.id = ?";
					con.query(query, req.params.id, function(err, rows2,fields) {
						if (err) throw err;
						row.OnItem = rows2[0];
						res.jsonp(rows0);
					});
				});
			});
		});
	});
});

app.get('/Review/delete/:id', function(req,res,next) {
	var id = req.params.id;
	con.query('DELETE FROM Review WHERE id = ?', id, function(err,rows) {
		if (err) { console.log('Error deleting'); }
		else {
			console.log('in delete success');
			res.jsonp(rows);
		}
	});
});

app.post('/Review/update/:id', function(req,res,next) {
	var id = req.params.id;
	var Review = '';
	req.on('data', function(data) {
		Review += data;
	});
	req.on('end', function() {
		Review = JSON.parse(Review);
		var data = {
			CreatedBy : Review.CreatedBy ,
			Content : Review.Content ,
			CreateDate : Review.CreateDate ,
			OnItem : Review.OnItem 
		};
		con.query('UPDATE Review set ? WHERE id = ?', [data, id],function(err,rows) {
			if (err) { console.log('Error updating'); }
			else {
				console.log('in update success');
				res.jsonp(true);
			}
		});
	});
});

app.post('/Review/add', function(req,res) {
	var Review = '';
	req.on('data', function(data) {
		Review += data;
	});
	req.on('end', function() {
		Review = JSON.parse(Review);
		var data = {
			CreatedBy : Review.CreatedBy ,
			Content : Review.Content ,
			CreateDate : Review.CreateDate ,
			OnItem : Review.OnItem 
		};
		con.query('INSERT INTO Review set ? ', data, function(err,rows) {
			if (err) { console.log('Error inserting'); }
			else {
				console.log('in insert success');
				res.jsonp(true);
			}
		});
	});
});

app.get('/User/find/OAuthID/:OAuthID', function(req,res) {
	totalCount = 0;
	count = 0;
	var query = "select User.ID , User.OAuthID , User.Info , User.Name  from User as User where User.OAuthID = ? ";
	con.query(query,[req.params.OAuthID], function(err, rows0,fields) {
		if(err) throw err;
		if (count == totalCount) {
			res.jsonp(rows0);
		}
	});
});


 /* Item: CRUD GET,DELETE,UPDATE,POST *NOT* BY ID */
app.get('/Item/find/Item/:Item', function(req,res) {
	totalCount = 0;
	count = 0;
	var query = "select Item.ID , Item.Description , Item.Price , Item.Title , Item.Quantity  from Item as Item where Item.ID = ? ";
	con.query(query,[req.params.Item], function(err, rows0,fields) {
		if(err) throw err;
		if (count == totalCount) {
			res.jsonp(rows0);
		}
	});
});

app.get('/Review/find/Item/:Item', function(req,res) {
	totalCount = 0;
	count = 0;
	var query = "select Review.ID , Review.CreatedBy , Review.Content , Review.CreateDate , Review.OnItem  from Review as Review where Review.OnItem = ? ";
	con.query(query,[req.params.Item], function(err, rows0,fields) {
		if(err) throw err;
		totalCount += rows0.length;
		rows0.forEach(function(row) { 
			query = "select User.ID , User.OAuthID , User.Info , User.Name ";
			query += "from  User as User  inner join Review as Review ";
			query += "where  User.id = Review.CreatedBy and Review.id = ?";
			con.query(query, row.ID, function(err, rows1,fields) {
				if (err) throw err;
				row.CreatedBy = rows1[0];
				rows0.forEach(function(row) { 
					query = "select Item.ID , Item.Description , Item.Price , Item.Title , Item.Quantity ";
					query += "from  Item as Item  inner join Review as Review ";
					query += "where  Item.id = Review.OnItem and Review.id = ?";
					con.query(query, row.ID, function(err, rows2,fields) {
						if (err) throw err;
						row.OnItem = rows2[0];
						count += 1;
						if (count == totalCount) {
							res.jsonp(rows0);
						}
					});
				});
			});
		});
		if (rows0.length == 0) {
			res.jsonp([])
		}
	});
});

app.get('/Similar/find/Item/:Item', function(req,res) {
	totalCount = 0;
	count = 0;
	var query = "select Similar.ID , Similar.Similar1 , Similar.Similar2  from Similar as Similar where Similar.Similar1 = ?  or Similar.Similar2 = ? ";
	con.query(query,[req.params.Item , req.params.Item], function(err, rows0,fields) {
		if(err) throw err;
		totalCount += rows0.length;
		rows0.forEach(function(row) { 
			query = "select Item.ID , Item.Description , Item.Price , Item.Title , Item.Quantity ";
			query += "from  Item as Item  inner join Similar as Similar ";
			query += "where  Item.id = Similar.Similar1 and Similar.id = ?";
			con.query(query, row.ID, function(err, rows1,fields) {
				if (err) throw err;
				row.Similar1 = rows1[0];
				rows0.forEach(function(row) { 
					query = "select Item.ID , Item.Description , Item.Price , Item.Title , Item.Quantity ";
					query += "from  Item as Item  inner join Similar as Similar ";
					query += "where  Item.id = Similar.Similar2 and Similar.id = ?";
					con.query(query, row.ID, function(err, rows2,fields) {
						if (err) throw err;
						row.Similar2 = rows2[0];
						count += 1;
						if (count == totalCount) {
							res.jsonp(rows0);
						}
					});
				});
			});
		});
		if (rows0.length == 0) {
			res.jsonp([])
		}
	});
});


 /* Home: CRUD GET,DELETE,UPDATE,POST *NOT* BY ID */
app.get('/Item', function(req,res) {
	totalCount = 0;
	count = 0;
	var query = "select Item.ID , Item.Description , Item.Price , Item.Title , Item.Quantity  from Item as Item where ";
	con.query(query,[], function(err, rows0,fields) {
		if(err) throw err;
		if (count == totalCount) {
			res.jsonp(rows0);
		}
	});
});


 /* Profile: CRUD GET,DELETE,UPDATE,POST *NOT* BY ID */
app.get('/User/find/User/:User', function(req,res) {
	totalCount = 0;
	count = 0;
	var query = "select User.ID , User.OAuthID , User.Info , User.Name  from User as User where User.ID = ? ";
	con.query(query,[req.params.User], function(err, rows0,fields) {
		if(err) throw err;
		if (count == totalCount) {
			res.jsonp(rows0);
		}
	});
});

app.get('/Review/find/User/:User', function(req,res) {
	totalCount = 0;
	count = 0;
	var query = "select Review.ID , Review.CreatedBy , Review.Content , Review.CreateDate , Review.OnItem  from Review as Review where Review.CreatedBy = ? ";
	con.query(query,[req.params.User], function(err, rows0,fields) {
		if(err) throw err;
		totalCount += rows0.length;
		rows0.forEach(function(row) { 
			query = "select User.ID , User.OAuthID , User.Info , User.Name ";
			query += "from  User as User  inner join Review as Review ";
			query += "where  User.id = Review.CreatedBy and Review.id = ?";
			con.query(query, row.ID, function(err, rows1,fields) {
				if (err) throw err;
				row.CreatedBy = rows1[0];
				rows0.forEach(function(row) { 
					query = "select Item.ID , Item.Description , Item.Price , Item.Title , Item.Quantity ";
					query += "from  Item as Item  inner join Review as Review ";
					query += "where  Item.id = Review.OnItem and Review.id = ?";
					con.query(query, row.ID, function(err, rows2,fields) {
						if (err) throw err;
						row.OnItem = rows2[0];
						count += 1;
						if (count == totalCount) {
							res.jsonp(rows0);
						}
					});
				});
			});
		});
		if (rows0.length == 0) {
			res.jsonp([])
		}
	});
});

var server = app.listen(config.api_port, function() {
	console.log('We have started our server on port '+config.api_port);
});

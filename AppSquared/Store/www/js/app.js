// We use an 'Immediate Function' to initialize the application to avoid leaving anything behind in the global scope
(function () {

    // Initate the library
    hello.init({
        google : '164737927993-l34g84brkg96ufe30ve2mjpg6lcen2pg.apps.googleusercontent.com',
        facebook : '160981280706879',
        windows : '00000000400D8578'
    }, {
        // Define the OAuth2 return URL
        redirect_uri : 'http://localhost:8080/index.html'//Must remove port to work in cordova
    });
    var serviceItem= new ItemService();
    var serviceUser= new UserService();
    var serviceSimilar= new SimilarService();
    var serviceReview= new ReviewService();
    
    router.addRoute('Item/Item/:Item', function(Item){
        serviceItem.findByItem(Item).done(function(ItemResponse){ 
        serviceReview.findByItem(Item).done(function(ReviewResponse){ 
        serviceSimilar.findByItem(Item).done(function(SimilarResponse){ 
            $('#container').html(new ItemView({VIEW: {Item:ItemResponse, Similar:SimilarResponse, Review:ReviewResponse },CREATE: "Review"}).render().$el);
        setLoginButton();
        });
        });
        });
        });
    
    router.addRoute('', function(){
        serviceItem.findAll().done(function(ItemResponse){ 
            $('#container').html(new HomeView({VIEW: {Item:ItemResponse },CREATE: "Item"}).render().$el);
        setLoginButton();
        });
        });
    
    router.addRoute('Profile/User/:User', function(User){
        serviceUser.findByUser(User).done(function(UserResponse){ 
        serviceReview.findByUser(User).done(function(ReviewResponse){ 
            $('#container').html(new ProfileView({VIEW: {User:UserResponse, Review:ReviewResponse }}).render().$el);
        setLoginButton();
        });
        });
        });
    
    router.start();
    
}());

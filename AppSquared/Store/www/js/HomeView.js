var HomeView = function(Params){
    this.template = function(){
        return new EJS({url:'templates/Home'}).render(Params);
    }

    this.render = function(){
        this.$el.html(this.template());
        return this;
    }

    this.initialize = function(){
        this.$el = $('<div/>');
    }

    this.initialize();
}
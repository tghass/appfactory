var ProfileView = function(Params){
    this.template = function(){
        return new EJS({url:'templates/Profile'}).render(Params);
    }

    this.render = function(){
        this.$el.html(this.template());
        return this;
    }

    this.initialize = function(){
        this.$el = $('<div/>');
    }

    this.initialize();
}
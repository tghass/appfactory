package com.cse379.appsquared;

import java.io.*;
import org.apache.commons.io.FileUtils;
import java.util.*;

public class CordovaGenerator{
    //TODO:
    // - Should use Maven resources for ref_dir and load from compiles JAR

    //////////
    //Fields//
    //////////
    private static final File REF_DIR = new File("resources/www");
    private static final String jsFolder = "js/";
    private static final String templateFolder = "templates/";
    private static final String appJs = jsFolder+"app.js";
    private static final String serviceFolder = jsFolder+"services/";
    
    private File outputDir;

    ////////////////
    //Constructors//
    ////////////////

    /** Constructor for CordovaGenerator */
    public CordovaGenerator(File out){
        outputDir=out;
        if (!outputDir.isDirectory()) {
			outputDir.mkdir();//Make the directory
		}
        //Clean the dir the copy over the reference directory
        try{
            FileUtils.cleanDirectory(outputDir);
            FileUtils.copyDirectory(REF_DIR,outputDir);
        }catch (IOException e){
			System.out.println("Error copying over reference dir for cordova");
        }
    }

    ///////////
    //Methods//
    ///////////
    public void createCode(HashMap<String,DataObj> dataObjsMap, 
        HashMap<String,PageObj> pageObjMap){
		
        createAppJs(pageObjMap, dataObjsMap);
        createViewFiles(pageObjMap, dataObjsMap);
        createTemplates(pageObjMap,dataObjsMap);
        createIndexHtml(pageObjMap,dataObjsMap);
        createServices(pageObjMap, dataObjsMap);
    }
    /* Create a service file for every Data Object*/
    private void createServices(HashMap<String, PageObj> pageObjsMap, 
		HashMap<String,DataObj> dataObjsMap){
		
        Iterator it = dataObjsMap.entrySet().iterator();
        ServiceGenerator servGen = new ServiceGenerator();
        	
			
		while(it.hasNext()){
            Map.Entry one = (Map.Entry)it.next();
            String name = (String)one.getKey();
            DataObj data = (DataObj)one.getValue();
            try{
				PrintWriter serviceWriter = new PrintWriter(
                        new BufferedWriter( new FileWriter(new File(outputDir,serviceFolder+name+"Service.js")))
                        );	
				serviceWriter.write(servGen.declareService(name));
				serviceWriter.write(servGen.genGetById(name));
				serviceWriter.write(servGen.genGetAll(name));
                serviceWriter.write(servGen.genAdd(name));
                serviceWriter.write(servGen.genUpdate(name));
                serviceWriter.write(servGen.genDelete(name));
				
				//Look for all instances of a page's SHOW and find by its params
				Iterator pageIt = pageObjsMap.entrySet().iterator();
				while (pageIt.hasNext()) {
					Map.Entry pOne = (Map.Entry)pageIt.next();
					PageObj curPageObj = (PageObj)pOne.getValue();
					List<Section> sections = curPageObj.getSections();
					for (Section section : sections) {
						if (section.getType() == Section.Type.VIEW && section.getShow().contains(name)) {
							serviceWriter.write(servGen.genGetByField(name,section.getParams()));
						}
					}
				}
                //Hack to get OAuth stuff working
                serviceWriter.write(servGen.genGetByField("User",new ArrayList<String>(Arrays.asList(new String[] {"OAuthID"}))));

				//Close file
				serviceWriter.write("}");
				serviceWriter.close();
               
            }catch(IOException e){
                System.out.println("\nCan't write Cordova code to file "+serviceFolder+name+"Service.js");
            }
        }
		
		
			
			
    }
	
    private void createIndexHtml(HashMap<String,PageObj> pageObjMap,HashMap<String,DataObj> dataObjsMap){
        try{
            File index = new File(outputDir,"index.html");
            PrintWriter indexWriter = new PrintWriter(
                    new BufferedWriter( new FileWriter(index))
                    );
            //Write beginning and head
            indexWriter.write("<!DOCTYPE html>\n"+
                              "<html>\n"+
                              "<head>\n"+
                              "\n"+
                              "    <meta charset=\"utf-8\">\n"+
                              "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0\">\n"+
                              "    <link href=\"assets/ratchet/css/ratchet.css\" rel=\"stylesheet\">\n"+
                              "    <link href=\"assets/style.css\" rel=\"stylesheet\">\n"+
                              "</head>\n\n");
            //Write body
            indexWriter.write("<body>\n"+
                              "\n"+
                              "    <script type=\"text/javascript\" src=\"cordova.js\"></script>\n"+
                              "    <script src=\"lib/hello.js\"></script>\n"+
                              "    <script src=\"lib/jquery.js\"></script>\n"+
                              "    <script src=\"lib/router.js\"></script>\n"+
                              "    <script src=\"lib/ejs.js\"></script>\n\n"+
                              "    <div id='container'></div>\n");
            //Include Views
            Iterator it = pageObjMap.entrySet().iterator();
            while(it.hasNext()){
                Map.Entry one = (Map.Entry)it.next();
                String name = (String)one.getKey();
                indexWriter.write("    <script src=\""+jsFolder+name+"View.js\"></script>\n");
            }
            indexWriter.write("\n");
            //Include services
            it = dataObjsMap.entrySet().iterator();
            while(it.hasNext()){
                Map.Entry one = (Map.Entry)it.next();
                String name = (String)one.getKey();
                indexWriter.write("    <script src=\""+serviceFolder+name+"Service.js\"></script>\n");
            }
            //Inlude login capabilities
            indexWriter.write("    <script src=\"js/Login.js\"></script>\n");
            //Include app.js
            indexWriter.write("    <script src=\""+appJs+"\"></script>\n");
            //End body and html
            indexWriter.write("</body>\n</html>");

            indexWriter.close();
        }catch(IOException e){
            System.out.println("\nCan't write Cordova code to file index.html");
        }
    }
    private void createViewTemplate(HashMap<String,PageObj> pageObjMap, HashMap<String,DataObj> dataObjsMap,
            String param, List<String> display, String base, PrintWriter writer){
        for(String di : display){
            if(di.startsWith("@"))
                writer.write("        <p>"+di.substring(1)+"</p>\n");
            else{
                List<Field> fieldsOfThisObj = dataObjsMap.get(param).getFields();
                int indOfField = -1;
                for(int i=0;i<fieldsOfThisObj.size();i++){//Find which field it is
                    if(fieldsOfThisObj.get(i).getName().equals(di))
                        indOfField=i;
                }
                if(indOfField !=-1){//It is a field
                    Field thisField = fieldsOfThisObj.get(indOfField);
                    if(thisField.getType()==Field.Type.FOREIGN_KEY){//It is a foregin key!
                        String objName = thisField.getTypeStr();
                        DataObj obj = dataObjsMap.get(objName);
                        createViewTemplate(pageObjMap,dataObjsMap,objName,obj.getDisplay(),base+"."+di,writer);
                    }
                    else{//Just a normal field
                        if(di.equals(dataObjsMap.get(param).getLinkField())){//Should be a link
                            String pageName = dataObjsMap.get(param).getLinkPage();
                            //Find params
                            StringBuilder params = new StringBuilder(64);//Add params to url
                            params.append((pageName.equals("Home") ? "" : pageName));
                            for(String parameter : pageObjMap.get(pageName).getParams()){
                                if(parameter.equals("LoggedInUser"))
                                    continue;
                                params.append("/"+param+"/<%="+base+".ID%>");
                            }
							
							int goTo = base.length() - base.indexOf("[");
                            writer.write("        <a href=\"#"+params.toString()+"\" class=\"view_"+base.substring(5,base.indexOf("["))+"_<%="+base+".ID %>\"><%="+base+"."+di+"%></a>\n");
                        }
                        else
                            writer.write("        <p class=\"view_"+base.substring(5,base.indexOf("["))+"_<%="+base+".ID %>\"><%="+base+"."+di+"%></p>\n");

                    }
                }
                else{//Find this obj
                    DataObj fieldDataObj = dataObjsMap.get(di);
                    if(fieldDataObj==null)
                        System.out.println("\nError, Field"+di+" is not a fields of the obj "+param);
                    Relation referencedRelation = fieldDataObj.getRelation(param);//Get relation of this field
                    if(referencedRelation==null){
                        System.out.println("\nError, Field"+di+" is not a field of the obj: "+param);
                    }else{
                        //TODO, test this with a a to b relation where a!=b
                        createViewTemplate(pageObjMap,dataObjsMap,
                                referencedRelation.getA(),
                                dataObjsMap.get(referencedRelation.getA()).getDisplay(),
                                base+"."+referencedRelation.getName()+1,
                                writer);
                        createViewTemplate(pageObjMap,dataObjsMap,
                                referencedRelation.getA(),
                                dataObjsMap.get(referencedRelation.getB()).getDisplay(),
                                base+"."+referencedRelation.getName()+2,
                                writer);
                    }
                }
            }
        }
    }
    private void createTemplates(HashMap<String,PageObj> pageObjMap,HashMap<String,DataObj> dataObjsMap){
        Iterator it = pageObjMap.entrySet().iterator();
        while(it.hasNext()){
            Map.Entry one = (Map.Entry)it.next();
            String name = (String)one.getKey();
            PageObj page = (PageObj)one.getValue();
            File thisTemplate = new File(outputDir,templateFolder+name+".ejs");
            try{
                PrintWriter templateWriter = new PrintWriter(
                        new BufferedWriter( new FileWriter(thisTemplate))
                        );
                //Header
                templateWriter.write("<header class=\"bar bar-nav\">\n"+
                                     "    <a id=\"home\" class=\"btn pull-left\" href=\"#\">Home</a>\n"+
                                     "    <a id=\"login\" class=\"btn pull-right\" >Log In</a>\n"+
                                     "    <h1 class=\"title\">"+name+"</h1>\n"+
                                     "</header>\n");
                //Body
                templateWriter.write("<div id=\"maincontent\" class=\"content\">\n");
                templateWriter.write("    <h1 class=\"changme\">Customize this page here:"+
                        outputDir.getPath()+"/"+templateFolder+name+
                        "</h1>\n");
                
				//Just output params for now
                for(String param : page.getShow(Section.Type.VIEW)){//each dataobj we will show
                    List<String> display = dataObjsMap.get(param).getDisplay();
                   DataObj d = dataObjsMap.get(param);
                    templateWriter.write("    <% for(var i=0;i<VIEW."+param+".length; i++){ %>\n");

                    //Use view and links to create the appropriate template
                    createViewTemplate(pageObjMap,dataObjsMap,param,display,"VIEW."+param+"[i]",templateWriter);
					
					//handle if this object is also deletable on this page
					if (page.getShow(Section.Type.DELETE).contains(param)){
						int count = 0;
						for (String delparam : page.getParams(Section.Type.DELETE)) {
							//Check if it is a module to filter by
							Modules m = new Modules();
							if (m.isModule(delparam)) {
								String tablename = m.modNameToTableName(delparam); //LoggedInUser will return UserException
								for (int fkIndex : d.indexForForeignKeysOfType(tablename)) {
									String foreignKeyField = d.getFieldName(fkIndex);
									//need the id at the end because it is a foreign key obj
									templateWriter.write("\t\t<% if ("+delparam+" == VIEW."+d.getName()+"[i]."+foreignKeyField+".ID) {%> \n");
									count +=1;
								}
							}
						}
						templateWriter.write("\t\t\t<button id='delete_button_<%=VIEW."+param+"[i].ID %>' class='delete"+param+"' type='button'>Delete "+param+"</button><br>\n");
						for (int i=0; i < count; i++) {
							templateWriter.write("\t\t<% } %>\n");
						}
					}
					
					//handle if modifiable
					if (page.getShow(Section.Type.MODIFY).contains(param)) {
					int count = 0;
						for (String modparam : page.getParams(Section.Type.MODIFY)) {
							//Check if it is a module to filter by
							Modules m = new Modules();
							if (m.isModule(modparam)) {
								String tablename = m.modNameToTableName(modparam); //LoggedInUser will return UserException
								for (int fkIndex : d.indexForForeignKeysOfType(tablename)) {
									String foreignKeyField = d.getFieldName(fkIndex);
									//need the id at the end because it is a foreign key obj
									templateWriter.write("\t\t<% if ("+modparam+" == VIEW."+d.getName()+"[i]."+foreignKeyField+".ID) {%> \n");
									count +=1;
								}
							}
						}
						templateWriter.write("\t\t\t<div id='<%=VIEW."+param+"[i].ID %>'> </div><br>\n");
						templateWriter.write("\t\t\t<button id='modify_button_<%=VIEW."+param+"[i].ID %>' class='modifybutton' type='button'>Modify "+param+"</button><br>\n");
						for (int i=0; i < count; i++) {
							templateWriter.write("\t\t <% } %>\n");
						}
					}
					
                    templateWriter.write("    <% } %>\n");//end for loop
                    templateWriter.write("    <br>\n\n");
                }
				
				//For each data object to be created, loop through the params
				//and see how they apply to the creation of the data object
                for(String show : page.getShow(Section.Type.CREATE)){

				
					//display create elements
                    templateWriter.write("    <p> Create "+show+"</p>\n");

					genFields(show,templateWriter,false,dataObjsMap,page);
					
					templateWriter.write("\t<button id='create"+show+"' type='button'>Add" + show + "</button>\n"); 
					templateWriter.write("\n");
					
				}
				
               
			   
                templateWriter.write("</div>");

                templateWriter.close();
            }catch(IOException e){
                System.out.println("\nCan't write Cordova code to file "+templateFolder+name+".ejs");
            }
        }
    }
	
	
	private void genFields(String show,PrintWriter templateWriter, boolean hidden,
	HashMap<String,DataObj> dataObjsMap, PageObj page) {
	
	String modifyId= "<%=VIEW."+show+"[i].ID%>";
			//Get all params for the object to be shown
					DataObj d = dataObjsMap.get(show);
					HashMap<String, String> fieldValues = new HashMap<String,String>();
					
			if (hidden) {
				for (Field f : d.getFields()) {
					String table = d.fieldNameToTableName(f);
					if (table != "") {
						fieldValues.put(table,f.getName());
					}
				}
			}
			else {
					for (String param : page.getParams(Section.Type.CREATE)) {
						// If param is a module, find out what table
						// it is related to and use that table to determine which
						// field of the data object it relates to
						List<Integer> fkIndices = d.indexForForeignKeysOfType(param);
						Modules m = new Modules();
						if (m.isModule(param)) {
							String tablename = m.modNameToTableName(param);
							fieldValues.put(tablename, param);
						}
						//Check if param is a foreign key dependency of dataobj
						else if (fkIndices.size() > 0) {		
							for (int fkIndex : fkIndices) {
								String foreignKeyField = d.getFieldName(fkIndex);
								fieldValues.put(param, "VIEW."+param+"[0].ID");
							}
						}
						//check if param is the same name as that object.
						else if (d.getName().equals(param)) {
							fieldValues.put(param,"VIEW."+param+"[0].ID");
						}
					}
					}
		for (Field f : d.getFields()) {
			//if it's a foreign key, this has a table else ""
			String tableName = d.fieldNameToTableName(f);

			//If it is of type date
			if (f.getTypeStr().equals("date")) {
				if (hidden) {
					templateWriter.write("\t\t\t$('<input/>').attr({ " +
								"id: 'modify"+f.getName()+ "',"+
								"value: Params.VIEW."+show+"[i]."+f.getName()+ ".split('T')[0]"+
								"}).appendTo(div);\n");
					templateWriter.write("\t\t\t$('<br>').appendTo(div);\n");
					
					} 
				else {
					templateWriter.write("\t<input id=create"+f.getName()+
								"  value='<%= (new Date()).toISOString().split('T')[0]%>'  disabled />\n");
								}
				continue;
			}
			//if it is a foreign key, see if associated param
			if (tableName.length() > 0) {
				if (hidden) {
					if (fieldValues.containsKey(tableName)) {
						templateWriter.write("\t\t\t$('<input/>').attr({ " +
								"id: 'modify"+f.getName()+ "',"+
								"type: 'hidden'," +
								"value:Params.VIEW."+show+"[i]."+f.getName()+".ID"+
								"}).appendTo(div);\n");
								
					templateWriter.write("\t\t\t$('<br>').appendTo(div);\n");
						continue;
					}
					else {
							templateWriter.write("\t\t\t$('<input/>').attr({ " +
								"id: 'modify"+f.getName()+ "',"+
								"value:Params.VIEW."+show+"[i]."+f.getName()+
								"}).appendTo(div);\n");
					templateWriter.write("\t\t\t$('<br>').appendTo(div);\n");
					}
				}
				else {
					if (fieldValues.containsKey(tableName)) {
						String value= fieldValues.get(tableName);
						
						templateWriter.write("\t<input id=create"+f.getName()+
									" type='hidden' value='<%="+value+"%>' />\n");
						continue;
					}
					else {
					templateWriter.write("\t<input id=create"+f.getName()+" type='text' value='"+
										show+" "+f.getName()+"' />\n");
					}
					
				}
			}
			//all others that aren't id
			else if (!f.getName().equals("ID")) { //id field is automatically generated
				if (hidden) {
						templateWriter.write("\t\t\t$('<input/>').attr({ " +
								"id: 'modify"+f.getName()+ "',"+
								"value:Params.VIEW."+show+"[i]."+f.getName()+
								"}).appendTo(div);\n");
					templateWriter.write("\t\t\t$('<br>').appendTo(div);\n");
				}
				else {
					templateWriter.write("\t<input id=create"+f.getName()+" type='text' value='"+
										show+" "+f.getName()+"' />\n");
				}
			}
		}
	}
	
	
	// CREATE VIEW.JS FILES----------------------
    private void createViewFiles(HashMap<String,PageObj> pageObjMap,
	HashMap<String,DataObj> dataObjsMap){
        Iterator it = pageObjMap.entrySet().iterator();
        while(it.hasNext()){
            Map.Entry one = (Map.Entry)it.next();
            String name = (String)one.getKey();
            PageObj page = (PageObj)one.getValue();
            File thisView = new File(outputDir,jsFolder+name+"View.js");
            try{
                PrintWriter viewWriter = new PrintWriter(
                        new BufferedWriter( new FileWriter(thisView))
                        );
                
                //Obj decl


		
				viewWriter.write("var "+name+"View = function(");
				//Determine which service has the correct function	
				//for each Show object
				StringBuilder serviceArgs = new StringBuilder(64);
				for (Section s: page.getSections()) {
					//Have to pass in the services to the view files that are going to
					//be called to add a dataobj
					if (s.getType() != Section.Type.VIEW) {
						for  (String serviceName : s.getShow()) {
							if(!serviceArgs.toString().contains(serviceName)){
								serviceArgs.append("service"+serviceName+",");
							}
						}
					}
				}
				viewWriter.write(serviceArgs.toString());
				viewWriter.write("Params){\n");

				//Global self variable
				viewWriter.write("\tvar _self = this; // in case in an event handler and this obj is overwritten\n");
                //Template func
                viewWriter.write("    this.template = function(){\n"+
                                 "        return new EJS({url:'templates/"+name+"'}).render(Params);\n");
                viewWriter.write("    }\n\n");
				
			
                //Render func
                viewWriter.write("    this.render = function(){\n"+
                                 "        this.$el.html(this.template());\n"+
                                 "        return this;\n"+
                                 "    }\n\n");
								 
				//EVENT--HANDLERS

				//(1) ADD
				
				//Add document on click button event handlers
				StringBuilder onClicksAdd = new StringBuilder(128);
				
				for(String param : page.getShow(Section.Type.CREATE)){
                   StringBuilder dataJSON = new StringBuilder(128);
				   DataObj d = dataObjsMap.get(param);
				   onClicksAdd.append("\t\t$(document).on('click','#create" +param+"'," +
									"this.add"+param+");\n");
				
					//generate the event handler functions
					viewWriter.write("\tthis.add"+param+"=function(e) {\n");
					viewWriter.write("\t\te.preventDefault();\n");
					viewWriter.write("\t\tvar data = {\n");
					for (Field f : d.getFields()) {
						if (!f.getName().equals("ID")) { //id field is automatically generated
							dataJSON.append("\t\t\t'"+f.getName()+"': $('#create"+f.getName()+"').val(),\n");
						}
					}
					//Remove last comma
					int indexOfLastComma = dataJSON.lastIndexOf(",");
					if(indexOfLastComma>=0) {
						dataJSON.setCharAt(indexOfLastComma,' ');
					}
					viewWriter.write(dataJSON.toString());
					viewWriter.write("\t\t};\n");
					viewWriter.write("\t\tservice"+param+ ".add"+ param+
									"(data).done(function(success) {\n" +
									"\t\t\tif (success) {console.log('Successfully added "+param+
									"');  \n" +
									"\t\t\talert('Successfully added "+param+"'); \n" +
									"\t\t\t$(window).trigger('hashchange');}\n"+
									"\t\t\t else { console.log('Unsuccessful add.'); }\n"+
									"\t\t});\n"+
									"\t};\n\n"); 
				}
				
				// (2) DELETE
				
				//Add document on click button event handlers
				StringBuilder onClicksDel = new StringBuilder(128);
				
				for(String param : page.getShow(Section.Type.DELETE)){
					onClicksDel.append("\t\t$(document).on('click','.delete" +param+"'," +
									"this.delete"+param+");\n");
				
					viewWriter.write("\tthis.delete"+param+"= function() {\n");
					viewWriter.write("\t\tvar id = (this.id).slice(14); // full id is delete_button_[id]\n");
					viewWriter.write("\t\tservice"+param+".del"+param+"(id).done(function(success) {\n");
					viewWriter.write("\t\t\tif (success) { \n alert('Delete success');\n");
					viewWriter.write("\t\t\t$(window).trigger('hashchange');}\n");
					viewWriter.write("\t\t\telse { console.log('Unsuccessful delete.'); }\n");
					viewWriter.write("\t\t});\n");
					viewWriter.write("\t}\n\n");
					
				}
				
				// (3) MODIFY
				
				//Add document on click button event handlers
				StringBuilder onClicksMod = new StringBuilder(128);
		
		
				for(String param : page.getShow(Section.Type.MODIFY)){
				  StringBuilder dataJSONmod = new StringBuilder(128);
				    DataObj d = dataObjsMap.get(param);
				 
					onClicksMod.append("\t\t$(document).on('click','.modifybutton'," +
									"this.makeVisible"+param+");\n");
					
					onClicksMod.append("\t\t$(document).on('click','.modify" +param+"'," +
									"this.modify"+param+");\n");
									
					//Must create 2 functions
					
					// (1) Generate input boxes
					viewWriter.write("\tthis.makeVisible" +param +" = function() {\n");
					//it is 14 because that gets the id after  modify_button_[ID]
					viewWriter.write("\t\tvar id = (this.id).slice(14); //get val after modify_button_\n");
					viewWriter.write("\t\t$('.view_"+param+"_'+id).hide(); //hides  \n");
					viewWriter.write("\t\t$(this).hide(); //hides  \n");
					viewWriter.write("\t\tvar div = $('#'+id); //accesses div to generate to  \n");
					viewWriter.write("\t\tfor (var i = 0; i < Params.VIEW."+param+".length; i++) {\n");
					viewWriter.write("\t\t\tif(Params.VIEW."+param+"[i].ID == id) {\n");
					genFields(param,viewWriter, true,dataObjsMap, page);
					viewWriter.write("\t\t\t$('<input/>').attr({id:'submit_"+param+"_'+id, 'class':'.modify"+param+"', style:'width:100px', type:'button', value:'submit'}).appendTo(div);\n");
					viewWriter.write("\t\t\t$(document).on('click','#submit_"+param+"_'+id, _self.modify"+param+"); \n");
					viewWriter.write("\t\t\t}\n");
					viewWriter.write("\t\t}\n");
					viewWriter.write("\t}\n");
					
					// (2) Submit input boxes
					viewWriter.write("\tthis.modify"+param+"= function() {\n");
					int len = 8+param.length();
					viewWriter.write("\t\tvar id = (this.id).slice("+len+"); //get val after modify_button_\n");
					viewWriter.write("\t\tvar data = {\n");
					for (Field f : d.getFields()) {
						if (!f.getName().equals("ID")) { //id field is automatically generated
							dataJSONmod.append("\t\t\t'"+f.getName()+"': $('#modify"+f.getName()+"').val(),\n");
						}
					}
					//Remove last comma
					int indexOfLastComma = dataJSONmod.lastIndexOf(",");
					if(indexOfLastComma>=0) {
						dataJSONmod.setCharAt(indexOfLastComma,' ');
					}
					dataJSONmod.append("\t\t\t};\n");
					viewWriter.write(dataJSONmod.toString());
					
					viewWriter.write("\t\tservice"+param+".update"+param+"(id,data).done(function(success) {\n");
					viewWriter.write("\t\t\tif (success) { alert('Update success');\n");
					viewWriter.write("\t\t\t$(window).trigger('hashchange');\n}\n");
					viewWriter.write("\t\t\telse { console.log('Unsuccessful update');}\n");
					viewWriter.write("\t\t});\n");
					viewWriter.write("\t}\n\n");
					
				}
				
                //Initialize func
                viewWriter.write("    this.initialize = function(){\n"+
                                 "        this.$el = $('<div/>');\n"+
								 "		$(document).off('click');\n"+
								  onClicksAdd.toString() + 
								  onClicksDel.toString() +
								  onClicksMod.toString() +
							   /* TODO add necessary code to get needed data */
                                 "    }\n\n");
                //Call initialize
                viewWriter.write("    this.initialize();\n");
                //End bracket
                viewWriter.write("}");

                viewWriter.close();
            }catch(IOException e){
                System.out.println("\nCan't write Cordova code to file "+jsFolder+name+"View.js");
            }
        }
    }
	
    private void createAppJs(HashMap<String,PageObj> pageObjMap, HashMap<String, DataObj> dataObjsMap){
        File appJsFile = new File(outputDir,appJs);
        try{
            PrintWriter appWriter = new PrintWriter(
                    new BufferedWriter( new FileWriter(appJsFile))
                    );
            //Begin immediate func
            appWriter.write("// We use an 'Immediate Function' to initialize the application to avoid leaving anything behind in the global scope\n"+
                            "(function () {\n"+
                            "\n"+
                            "    // Initate the library\n"+
                            "    hello.init({\n"+
                            //TODO: Parse Oauth tokens from a config file
                            "        google : '164737927993-l34g84brkg96ufe30ve2mjpg6lcen2pg.apps.googleusercontent.com',\n"+
                            "        facebook : '160981280706879',\n"+
                            "        windows : '00000000400D8578'\n"+
                            "    }, {\n"+
                            "        // Define the OAuth2 return URL\n"+
                            "        redirect_uri : 'http://localhost:8080/index.html'//Must remove port to work in cordova\n"+
                            "    });\n");
							
            //Initialize all services
			Iterator it = dataObjsMap.entrySet().iterator();
			while(it.hasNext()){
				Map.Entry one = (Map.Entry)it.next();
				String name = (String)one.getKey();
				DataObj data = (DataObj)one.getValue();
				appWriter.write("    var service"+name+"= new "+name+"Service();\n");
			}
			
            //Add routes
            it = pageObjMap.entrySet().iterator();
            while(it.hasNext()){
                Map.Entry one = (Map.Entry)it.next();
                String pageName = (String)one.getKey();
                PageObj page = (PageObj)one.getValue();
                //Write out this route
                appWriter.write("    \n"+
                                "    router.addRoute('");
                StringBuilder params = new StringBuilder(64);//Add params to url
                params.append((pageName.equals("Home") ? "" : pageName));

                for(String param : page.getParams()){
                    if(param.equals("LoggedInUser"))
                        continue;
                    params.append("/"+param+"/:"+param);
                }
                appWriter.write(params.toString());
                appWriter.write("', function(");
                params = new StringBuilder(64);//Do the params
                for(String param : page.getParams()){
                    if(param.equals("LoggedInUser"))
                        continue;
                    params.append(param+",");
                }
                int indexOfLastComma = params.lastIndexOf(",");
                if(indexOfLastComma>=0)
                    params.deleteCharAt(indexOfLastComma);//Remove last ,
                appWriter.write(params.toString());
                appWriter.write("){\n");
			
				int numQueries = 0;
				StringBuilder serviceArgs = new StringBuilder(64);
				//Determine which service has the correct function	
				//for each Show object
				for (Section s: page.getSections()) {
					if ( s.getType() == Section.Type.VIEW) {
						for (String serviceName: s.getShow()) {
							// Call the service function that queries the database for data obj based on param
							if (page.getViewParams().size() > 0) { 
                                for(String param : page.getViewParams()){
                                    appWriter.write("        service"+serviceName+"."); 
                                    appWriter.write("findBy"+param+"("+param+").done(function("+serviceName+"Response){ \n");
                                    numQueries++;
                                }
                            }
                            else{//No parameters
                                appWriter.write("        service"+serviceName+".findAll().done(function("+serviceName+"Response){ \n"); 
                                numQueries++;
                            }
						}
					}
					//Have to pass in the services to the view files that are going to
					//be called to add/del/modify a dataobj
					else {
						for  (String serviceName : s.getShow()) {
							if (!serviceArgs.toString().contains(serviceName)) {
								serviceArgs.append("service"+serviceName+",");
							}
						}	
					}
				}
				
				
				
				
				// need to use appended text so that incoming query variable
				// does not get confused with query response
				appWriter.write("            $('#container').html(new "+pageName+"View("+
								serviceArgs.toString()+
								page.getShowString("Response")+
                                ").render().$el);\n"+
                                "        setLoginButton();\n");

								
				//only add closing brackets for the params that we are querying against	
				for (int i = 0; i < numQueries; i++) {
					appWriter.write("        });\n");
				}
				appWriter.write("        });\n");
            }
            appWriter.write("    \n"+
                            "    router.start();\n"+
                            "    \n"+
                            "}());\n");
            appWriter.close();
        }catch(IOException e){
            System.out.println("\nCan't write Cordova code to file "+appJs);
        }
    }
}

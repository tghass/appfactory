var FriendshipService = function(){

    var baseUrl = 'http://localhost:3000/';
    this.findById = function(id){
        var deferred = $.Deferred();
        var ret = [];
        if(id===undefined){
            deferred.resolve(ret);
            return deferred.promise();
        }
        var url = baseUrl+'Friendship/find/'+ id;
        $.ajax({
            url: url,
            success: function(data) {
                $.each(data, function(key, val) {
                    ret.push(val);
                });
                deferred.resolve(ret);
            },
            dataType: 'jsonp',
            error: function(xhr, status, error) {
                console.log( xhr.responseText);
                deferred.reject("Transaction Error: ");
            }
        }); 
        return deferred.promise();
    }
//POST REQUESTS 
this.addFriendship = function(data){
        var deferred = $.Deferred();
        if(data===undefined){
            deferred.resolve(false);
            return deferred.promise();
        }
        var url = baseUrl+'Friendship/add';
        var data = JSON.stringify(data);
        $.ajax({
            url: url,
            type: 'post',
            crossDomain: true,
            dataType: 'json',
            contentType: 'application/json; charset=UTF-8',
            data: data,
            success: function(res) {
                console.log('Add successful');
                deferred.resolve(true);
            },
            error: function(xhr, status, error) {
                console.log( xhr.responseText);
                deferred.resolve(false);
            }
        }); 
        return deferred.promise();
    }
//UPDATE REQUESTS 
this.updateFriendship = function(id,data){
        var deferred = $.Deferred();
        if(id===undefined){
            deferred.resolve(false);
            return deferred.promise();
        }
        var url = baseUrl+'Friendship/update/'+id;
        var data = JSON.stringify(data);
        $.ajax({
            url: url,
            type: 'post',
            crossDomain: true,
            dataType: 'json',
            contentType: 'application/json; charset=UTF-8',
            data: data,
            success: function(res) {
                console.log('Update successful');
                deferred.resolve(true);
            },
            error: function(xhr, status, error) {
                console.log( xhr.responseText);
                deferred.resolve(false);
            }
        }); 
        return deferred.promise();
    }
//DELETE REQUESTS 
this.delFriendship = function(id){
        var deferred = $.Deferred();
        if(id===undefined){
            deferred.resolve(false)
            return deferred.promise();
        }
        var url = baseUrl+'Friendship/delete/'+id;
        var data = JSON.stringify(data);
        $.ajax({
            url: url,
            dataType: 'jsonp',
           success: function(data) {
                console.log('Delete successful');
                deferred.resolve(true);
            },
            error: function(xhr, status, error) {
                console.log( xhr.responseText);
                deferred.resolve(false);n            }
        }); 
        return deferred.promise();
    }
    this.findByLoggedInUser = function(LoggedInUser ){
        var deferred = $.Deferred();
        if( LoggedInUser===undefined  ){
            deferred.resolve({});
            return deferred.promise();
        }
        var url = baseUrl+'Friendship'+'/find/LoggedInUser/'+LoggedInUser;
        var ret = [];
        $.ajax({
            url: url,
            success: function(data) {
                $.each(data, function(key, val) {
                    ret.push(val);
                });
                deferred.resolve(ret);
            },
            dataType: 'jsonp',
            error: function(xhr, status, error) {
                console.log( xhr.responseText);
                deferred.reject("Transaction Error: ");
            }
        }); 
        return deferred.promise();
    }
    this.findByOAuthID = function(OAuthID ){
        var deferred = $.Deferred();
        if( OAuthID===undefined  ){
            deferred.resolve({});
            return deferred.promise();
        }
        var url = baseUrl+'User'+'/find/OAuthID/'+OAuthID;
        var ret = [];
        $.ajax({
            url: url,
            success: function(data) {
                $.each(data, function(key, val) {
                    ret.push(val);
                });
                deferred.resolve(ret);
            },
            dataType: 'jsonp',
            error: function(xhr, status, error) {
                console.log( xhr.responseText);
                deferred.reject("Transaction Error: ");
            }
        }); 
        return deferred.promise();
    }
}
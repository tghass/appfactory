var PostInfoView = function(Post  ){
    this.template = function(){
        return new EJS({url:'templates/PostInfo'}).render({VIEW:Post.VIEW}  );
    }

    this.render = function(){
        this.$el.html(this.template());
        return this;
    }

    this.initialize = function(){
        this.$el = $('<div/>');
    }

    this.initialize();
}
// We use an 'Immediate Function' to initialize the application to avoid leaving anything behind in the global scope
(function () {

    // Initate the library
    hello.init({
        google : '164737927993-l34g84brkg96ufe30ve2mjpg6lcen2pg.apps.googleusercontent.com',
        facebook : '160981280706879',
        windows : '00000000400D8578'
    }, {
        // Define the OAuth2 return URL
        redirect_uri : 'http://localhost:8080/index.html'//Must remove port to work in cordova
    });
    var serviceFriendship= new FriendshipService();
    var serviceComment= new CommentService();
    var serviceUser= new UserService();
    var servicePost= new PostService();
    
    router.addRoute('PostInfo/Post/:Post', function(Post){
        servicePost.findByPost(Post).done(function(PostResponse){ 
        serviceComment.findByPost(Post).done(function(CommentResponse){ 
            $('#container').html(new PostInfoView({VIEW: {Comment:CommentResponse, Post:PostResponse }}).render().$el);
        setLoginButton();
        });
        });
        });
    
    router.addRoute('', function(){
        serviceUser.findByLoggedInUser(LoggedInUser).done(function(UserResponse){ 
        serviceFriendship.findByLoggedInUser(LoggedInUser).done(function(FriendshipResponse){ 
            $('#container').html(new HomeView({VIEW: {Friendship:FriendshipResponse, User:UserResponse },CREATE: "Post"}).render().$el);
        setLoginButton();
        });
        });
        });
    
    router.addRoute('Profile/User/:User', function(User){
        serviceUser.findByUser(User).done(function(UserResponse){ 
        servicePost.findByUser(User).done(function(PostResponse){ 
            $('#container').html(new ProfileView({VIEW: {User:UserResponse, Post:PostResponse }}).render().$el);
        setLoginButton();
        });
        });
        });
    
    router.start();
    
}());

var HomeView = function(LoggedInUser  ){
    this.template = function(){
        return new EJS({url:'templates/Home'}).render({VIEW:LoggedInUser.VIEW}  );
    }

    this.render = function(){
        this.$el.html(this.template());
        return this;
    }

    this.initialize = function(){
        this.$el = $('<div/>');
    }

    this.initialize();
}
var ProfileView = function(User  ){
    this.template = function(){
        return new EJS({url:'templates/Profile'}).render({VIEW:User.VIEW}  );
    }

    this.render = function(){
        this.$el.html(this.template());
        return this;
    }

    this.initialize = function(){
        this.$el = $('<div/>');
    }

    this.initialize();
}
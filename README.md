# README #

This repository is for AppFactory, a program made by Adam Kafka and 
Tamara Hass for Senior Project at Lehigh University 2016. 

### What is AppFactory? ###

* Describe here

### How do I use AppFactory? ###

1. Make Config file (examples are .json files)
2. Run program's Makefile
   - Put your json file name in the Makefile
   - type: make && make run
3. Enjoy your app! CHeck out the contents of the Output folder
   - Install node js, and type: node app.js
   - We use the following modules from node, if you don't have them type:
           npm install express
           npm install express-cors
           npm install mysql

### How do I leverage Cordova? ###

* Use www/ dir, install plugin cordova-plugin-inappbrowser

### Who do I talk to? ###

Adam Kafka (mailto:adkafka@gmail.com) 

Tamara Hass (mailto:tgh216@lehigh.edu)